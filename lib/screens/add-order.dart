import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart' as geo;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'dart:convert';
import '../provider/categories_provider.dart';
import '../provider/order-provider.dart';
import '../services/utils.dart';
import '../theme/colors.dart';
import 'map_pick_location_screen.dart';

class AddNewOrder extends StatefulWidget {
  const AddNewOrder({Key? key}) : super(key: key);

  @override
  State<AddNewOrder> createState() => _AddNewOrderState();
}

class _AddNewOrderState extends State<AddNewOrder> {
  LatLng? _pickedLocation;
  LocationData? currentLocation;
  bool isInputEnable=true;


  Future<void> getAddressFromLatLng(LatLng latLng) async {
    try {
      String apiKey = 'AIzaSyDCSt4ABayMg8O3n9Hvxb_vrs_1oUfWXuA'; // استبدل YOUR_API_KEY بمفتاح API الخاص بك
      String url =
          'https://maps.googleapis.com/maps/api/geocode/json?latlng=${latLng.latitude},${latLng.longitude}&key=$apiKey';

      http.Response response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        Map<String, dynamic> data = json.decode(response.body);
        if (data['status'] == 'OK') {
          List<dynamic> results = data['results'];
          if (results.isNotEmpty) {
            List<dynamic> addressComponents =
            results[0]['address_components'];

            String country = _getAddressComponent(addressComponents, 'country');
            String city = _getAddressComponent(addressComponents, 'administrative_area_level_1');
            String address = results[0]['formatted_address'];

            print('Country: $country');
            print('City: $city');
            print('Address: $address');
          }
        }
      }
    } catch (e) {
      print(e);
    }
  }

  String _getAddressComponent(
      List<dynamic> addressComponents, String componentType) {
    for (var component in addressComponents) {
      List<dynamic> types = component['types'];
      if (types.contains(componentType)) {
        return component['long_name'];
      }
    }
    return '';
  }


  Future<void> _selectOnMap() async {
    currentLocation=await locationService.getCurrentLocation();

    final selectedLocation = await Navigator.of(context).push<LatLng>(
      MaterialPageRoute(
        builder: (ctx) => MapToPickLocationScreen(),
      ),
    );
    setState(() {
      getAddressFromLatLng(selectedLocation!);
      _pickedLocation = selectedLocation;

    });


  }



  @override
  Widget build(BuildContext context) {

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Consumer<CategoriesProvider>(
        builder: (context, catProvider, _)
    =>
      Consumer<OrderProvider>(
          builder: (context, orderProvider, _) =>
              Scaffold(
                appBar: AppBar(
                  title: Text('إضافة طلب',style: TextStyle(color: Colors.white)),
                  backgroundColor: kTeal400,
                ),
                body:catProvider.loadedItems.isEmpty
                    ? Center(
                    child: Container(
                        child: CircularProgressIndicator(
                          color: kTeal400,
                        )))
                    :
                SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  child: Center(
                    child: Form(
                      key: orderProvider.addOrderForm,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .02,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * .7,
                            child: TextFormField(
                              controller: orderProvider.productName,
                              style: TextStyle(height: 2),
                              keyboardType: TextInputType.text,
                              cursorColor: kTeal400,
                              decoration: InputDecoration(
                                labelText: 'اسم المنتج',
                                hintText: '',
                                fillColor: Colors.white,
                                // filled: true,
                              ),
                              validator: (value) {
                                value = orderProvider.productName.text;
                                if (value.isEmpty) {
                                  return "هذا الحقل مطلوب";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .02,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * .7,
                            // height: MediaQuery.of(context).size.height * 0.09,
                            child: TextFormField(
                              enabled:orderProvider. isInputEnable,
                              controller: orderProvider.country,
                              style: TextStyle(height: 2),
                              keyboardType: TextInputType.text,
                              cursorColor: kTeal400,
                              decoration: InputDecoration(
                                labelText: 'البلد',
                                hintText: '',
                                fillColor: Colors.white,
                                //    filled: true,
                              ),
                              validator: (value) {
                                value = orderProvider.country.text;
                                if (value.isEmpty) {
                                  return "هذا الحقل مطلوب";
                                } else {
                                  return null;
                                }
                              },
                              onSaved: (value) {},
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .02,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * .7,
                            // height: MediaQuery.of(context).size.height * 0.06,
                            child: TextFormField(
                              enabled:orderProvider. isInputEnable,
                              controller: orderProvider.city,
                              style: TextStyle(height: 2),
                              keyboardType: TextInputType.text,
                              cursorColor: kTeal400,
                              decoration: InputDecoration(
                                labelText: 'المدينة',
                                hintText: '',
                                fillColor: Colors.white,
                                //   filled: true,
                              ),
                              validator: (value) {
                                value = orderProvider.city.text;
                                if (value.isEmpty) {
                                  return "هذا الحقل مطلوب";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .02,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * .7,
                            child: TextFormField(
                              enabled:orderProvider. isInputEnable,
                              controller: orderProvider.address,
                              style: TextStyle(height: 2),
                              keyboardType: TextInputType.text,
                              cursorColor: kTeal400,
                              decoration: InputDecoration(
                                labelText: 'العنوان',
                                hintText: '',
                                fillColor: Colors.white,
                              ),
                              validator: (value) {
                                value = orderProvider.address.text;
                                if (value.isEmpty) {
                                  return "هذا الحقل مطلوب";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                          DropdownButton<dynamic>(
                            hint: Text(catProvider.addProhintList),
                            items: catProvider.loadedItems.map((e) {
                              return DropdownMenuItem(
                                child: Text(e.name!),
                                value: e,
                              );
                            }).toList(),
                            onChanged: (val) {
                              catProvider.changeHint(val);
                            },
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .02,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(5),
                                bottomRight: Radius.circular(5),
                                topLeft: Radius.circular(5),
                                bottomLeft: Radius.circular(5),
                              ),
                              color: Colors.white54,
                            ),
                            width: MediaQuery.of(context).size.width * .7,
                            height: MediaQuery.of(context).size.height * 0.15,
                            child: TextFormField(
                              controller: orderProvider.productDescription,
                              style: TextStyle(height: 3),
                              keyboardType: TextInputType.multiline,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                ),
                                labelText: 'وصف المنتج',
                                hintText: '',
                                fillColor: Colors.white,
                                filled: true,
                              ),
                              validator: (value) {
                                value = orderProvider.productDescription.text;
                                if (value.isEmpty) {
                                  return "هذا الحقل مطلوب";
                                }

                                return null;
                              },
                              onSaved: (value) {},
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .02,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              TextButton.icon(
                                onPressed: ()async{

                                    currentLocation=await locationService.getCurrentLocation();

                                    final selectedLocation = await Navigator.of(context).push<LatLng>(
                                      MaterialPageRoute(
                                        builder: (ctx) => MapToPickLocationScreen(),
                                      ),
                                    );

                                      String apiKey = 'AIzaSyDCSt4ABayMg8O3n9Hvxb_vrs_1oUfWXuA'; // استبدل YOUR_API_KEY بمفتاح API الخاص بك
                                      String url =
                                          'https://maps.googleapis.com/maps/api/geocode/json?latlng=${selectedLocation?.latitude??0.0},${selectedLocation?.longitude??0.0}&key=$apiKey';
                                      customLoader();
                                      http.Response response = await http.get(Uri.parse(url));
                                      BotToast.closeAllLoading();
                                      if (response.statusCode == 200) {
                                        Map<String, dynamic> data = json.decode(response.body);
                                        if (data['status'] == 'OK') {
                                          List<dynamic> results = data['results'];
                                          if (results.isNotEmpty) {
                                            List<dynamic> addressComponents =
                                            results[0]['address_components'];

                                            String country = _getAddressComponent(addressComponents, 'country');
                                            String city = _getAddressComponent(addressComponents, 'administrative_area_level_1');
                                            String address = results[0]['formatted_address'];
                                           orderProvider. isInputEnable=false;
                                            orderProvider.country.text=country;
                                            orderProvider.city.text=city;
                                            orderProvider.address.text=address;
                                            print('Country: $country');
                                            print('City: $city');
                                            print('Address: $address');
                                          }
                                        }
                                      }
                                      setState(() {
                                        _pickedLocation = selectedLocation;

                                      });






                                },
                                icon: Icon(
                                  Icons.pin_drop_rounded,
                                  color: _pickedLocation == null
                                      ? Colors.red
                                      : Colors.green,
                                ),
                                label: _pickedLocation == null
                                    ? Text(
                                  'حدّد موقعك على الخريطة',
                                  style: TextStyle(
                                      color: Colors.blue),
                                )
                                    : Text('تم تحديد الموقع '),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .02,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                                topLeft: Radius.circular(20),
                                bottomLeft: Radius.circular(20),
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.white,
                                  spreadRadius: 1,
                                  blurRadius: 1,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ],
                            ),
                            width: MediaQuery.of(context).size.width * .7,
                            height: MediaQuery.of(context).size.height * .06,
                            child: ElevatedButton(
                              child: Text("نشر",
                                  style: TextStyle(
                                    color: kGrey200,
                                    fontSize: 20,
                                  )),
                              onPressed: () async {
                                // productProvider.image1 = image;
                                // Validate returns true if the form is valid, or false otherwise.
                                if (orderProvider.addOrderForm.currentState!
                                    .validate()) {
                                  await orderProvider.addOOrder(title: orderProvider.productName.text,
                                      description: orderProvider.productDescription.text,
                                      categoryId: catProvider.categoryId==null?20:catProvider.categoryId!,
                                      country: orderProvider.country.text,
                                      city: orderProvider.city.text,
                                      address: orderProvider.address.text,
                                    alat: currentLocation==null?null: currentLocation!.latitude,
                                    alng: currentLocation==null?null:currentLocation!.longitude,
                                    clat: _pickedLocation==null?null:_pickedLocation!.latitude,
                                    clng: _pickedLocation==null?null:_pickedLocation!.longitude,
                                  ).then((value) {
                                    value.fold((l) {
                                      BotToast.closeAllLoading();
                                      AwesomeDialog(
                                        context: context,
                                        animType: AnimType.TOPSLIDE,
                                        dialogType: DialogType.SUCCES,
                                        title: "فشل العملية",
                                        body: Directionality(
                                          textDirection:
                                          TextDirection.rtl,
                                          child: Container(
                                            padding: EdgeInsets.all(8),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "حدث خطأ ما",
                                              style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 22,
                                                fontWeight:
                                                FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                        btnOkColor: Colors.red,
                                        btnOkOnPress: () {},
                                      ).show();

                                    }, (r) {
                                      BotToast.closeAllLoading();
                                      orderProvider.productName.text="";
                                      orderProvider.productDescription.text="";
                                      catProvider.categoryId=null;
                                      catProvider.addProhintList=" اختر الفئة";
                                      orderProvider.country.text="";
                                      orderProvider.city.text="";
                                      orderProvider.address.text="";
                                      orderProvider.isInputEnable=true;

                                      currentLocation=null;
                                      _pickedLocation=null;
                                      setState(() {

                                      });

                                      AwesomeDialog(
                                        context: context,
                                        animType: AnimType.TOPSLIDE,
                                        dialogType: DialogType.SUCCES,
                                        title: "تمت العملية",
                                        body: Directionality(
                                          textDirection:
                                          TextDirection.rtl,
                                          child: Container(
                                            padding: EdgeInsets.all(8),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "تم اضافة طلب بنجاح",
                                              style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 22,
                                                fontWeight:
                                                FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                        btnOkColor: Colors.red,
                                        btnOkOnPress: () {},
                                      ).show();
                                    });
                                  });
                                  // print("ready for send request ");
                                  // print("add product request start ");
                                  // await orderProvider.addOrder();
                                  // if (orderProvider.statusCodeAddOrder == 200) {
                                  //   AwesomeDialog(
                                  //     context: context,
                                  //     animType: AnimType.TOPSLIDE,
                                  //     dialogType: DialogType.SUCCES,
                                  //     title: "تمت العملية",
                                  //     body: Directionality(
                                  //       textDirection: TextDirection.rtl,
                                  //       child: Container(
                                  //         padding: EdgeInsets.all(8),
                                  //         alignment: Alignment.center,
                                  //         child: Text(
                                  //           "تم اضافة طلب بنجاح",
                                  //           style: TextStyle(
                                  //             color: Colors.grey,
                                  //             fontSize: 22,
                                  //             fontWeight: FontWeight.bold,
                                  //           ),
                                  //         ),
                                  //       ),
                                  //     ),
                                  //     btnOkColor: Colors.red,
                                  //     btnOkOnPress: () {},
                                  //   ).show();
                                  // }
                                  //
                                  // if (orderProvider.statusCodeAddOrder != 200) {
                                  //   AwesomeDialog(
                                  //     context: context,
                                  //     animType: AnimType.TOPSLIDE,
                                  //     dialogType: DialogType.ERROR,
                                  //     title: "فشل العملية",
                                  //     body: Directionality(
                                  //       textDirection: TextDirection.rtl,
                                  //       child: Container(
                                  //         padding: EdgeInsets.all(8),
                                  //         alignment: Alignment.center,
                                  //         child: Text(
                                  //           "${orderProvider.filedMessage}",
                                  //           style: TextStyle(
                                  //             color: Colors.grey,
                                  //             fontSize: 22,
                                  //             fontWeight: FontWeight.bold,
                                  //           ),
                                  //         ),
                                  //       ),
                                  //     ),
                                  //     btnOkColor: Colors.red,
                                  //     btnOkOnPress: () {},
                                  //   ).show();
                                  // }
                                  // print("add product request end ");
                                } else {
                                  print("not ready yet");
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor: kTeal400,
                                minimumSize: const Size(300, 75),
                                maximumSize: const Size(300, 75),
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                ),
              )),
    ),);
  }
}
