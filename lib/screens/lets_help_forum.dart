import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../theme/colors.dart';

//import 'package:webview_flutter_android/webview_flutter_android.dart';
// Import for iOS features.
class LestHelpForum extends StatefulWidget {
  LestHelpForum({Key? key}) : super(key: key);

  @override
  State<LestHelpForum> createState() => _LestHelpForumState();
}

class _LestHelpForumState extends State<LestHelpForum> {
  // final Completer<WebViewController> _controller =
  //     Completer<WebViewController>();

  bool _isLoading = true;
  final _key = UniqueKey();
  // late final WebViewController _controller;
  // @override
  // void initState(){
  //   _controller = WebViewController()
  //     ..loadRequest(
  //       Uri.parse('https://forum.letshelp-platform.com/'),
  //     );
  //   super.initState();
  //
  // }

  late ControllerPage controller;
  late WebViewController webViewController;
  RxBool isLoading=true.obs;
  @override
  void initState() {

  controller=Get.put(ControllerPage());
  webViewController=WebViewController()

    ..setJavaScriptMode(JavaScriptMode.unrestricted)
    ..setBackgroundColor(const Color(0x00000000))
    ..setNavigationDelegate(
      NavigationDelegate(
        onProgress: (int progress) {
          // Update loading bar.
        },

        onPageStarted: (String url) {

        },
        onPageFinished: (String url) {
          isLoading.value=false;
        },
        onWebResourceError: (WebResourceError error) {},
        onNavigationRequest: (NavigationRequest request) {
          if (request.url.startsWith('https://www.youtube.com/')) {
            return NavigationDecision.prevent;
          }
          return NavigationDecision.navigate;
        },
      ),
    )
    ..loadRequest(Uri.parse('https://www.letshelp-platform.com/forum'),headers: {
      'Authorization': 'Bearer ${ controller.token!.value}'
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {



    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: kTeal400,
          elevation: 3.0,
          title: Text(
            'المنتدى',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
          body:
         // _isLoading
          //     ? Center(
          //   child: CircularProgressIndicator(),
          // )
          //     :
        // InAppWebView(
        //   initialUrlRequest: URLRequest(
        //     url: Uri.parse("https://www.letshelp-platform.com/forum"),
        //     headers: {
        //       'token': '${token}'
        //     }
        //   ),
        // )

          Obx(() {
            return isLoading.value?
            Center(
                child: CircularProgressIndicator(
                  color:kTeal400 ,
                ),
              )
                :
              WebViewWidget(

                controller: webViewController

            );
          })
      ),
    );
  }
}


class ControllerPage extends GetxController {
  SharedPreferences? prefs;
  RxString? token="".obs ;
  @override
  void onInit()async {
    print(token);
     prefs = await SharedPreferences.getInstance();
     token!.value = prefs!.getString('token')!;
    super.onInit();
  }
}
