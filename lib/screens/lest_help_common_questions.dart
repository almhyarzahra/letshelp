import 'package:flutter/material.dart';
import 'package:letshelp/theme/colors.dart';

class common extends StatefulWidget {
  @override
  _common createState() => _common();
}

class _common extends State<common> {
  List<ExpansionItem> items=<ExpansionItem>[
    ExpansionItem(header: 'ما هي منصة فلنتساعد باختصار؟',body:"هي مواقع تواصل اجتماعي يخص عمل الخير والعمل الإنساني، بداية متخصص بالأفراد وتبادل الأشياء فيما بينهم، فكثير منا لديه أشياء لا تلزمه، وآخرون لديهم أشياء يحتاجونها، والمنصة تجمع بين هؤلاء مبدأيا، والفكرة ستتطور تدريجيا بناء على تطورات تفاعل الأفراد مع المنص"),
    ExpansionItem(header:'لماذا منصة فلنتساعد',body: 'حتى الآن نحن عبارة عن منصة رقمية فقط ولسنا منظمة أو جمعية غير ربحية ولا نعمل تحت أي مظلة حكومية أو غير حكومي'),
    ExpansionItem(header:'هل منصة فلنتساعد منظمة؟' ,body:'حتى الآن نحن عبارة عن منصة رقمية فقط ولسنا منظمة أو جمعية غير ربحية ولا نعمل تحت أي مظلة حكومية أو غير حكومية'),
    ExpansionItem(header:'ماهو مستقبل المنصة؟',body:'ننا نطمح أن تصبح المنصة مرجعية في توزيع فوائد العمل الإنساني بصورة عادلة، وأن تساهم في عملية تقييم الاحتياجات للفاعلين في المجال الخيري، إننا اذ بدأنا على نطاق الأفراد، لكن نخطط بعد ذلك لأن نصبح منصة تعمل تحت مظلة رسمية وبتراخيص في عدة دول وخاصة تلك التي تحوي أعداد كبيرة من المحتاجين والمعطاءين'),
    ExpansionItem(header:'فرق المنصة على الأرض',body:"لايوجد لدينا حاليا أي شخص يتعامل مع الحالات على أرض الواقع، ولكن موجود ضمن مخططاتنا التعاقد مع فرق تتأكد من الحالات مستقبلا وخاصة عند الانتقال من مرحلة تأمين الاحتياجات الفردية الصغيرة الى التوسع في العمل الخيري الإنساني"),
    ExpansionItem(header:'التقارير الدورية', body:'لأننا نجد نفسنا جزء من منظومة العمل الإنسانيفإننا سنصدر تقارير دورية عن الاحتياجات المسجلة في المنصة وذلك لتساعد الجمعيات والمنظمات الفاعلة بهذا المجال لتوجيه بوصلتها وللمساهمة في تقييم الاحتياجات'),
    ExpansionItem(header:'الموقع',body:'يتوفر للمنصة موقع على الهاتف يسمح للأشخاص المساهمة في تأمين احتياجات الآخر وكذلك للمحتاج طلب حاجته من خلال الموبايل وليس الكمبيوتر فقط'),
    ExpansionItem(header:'جمع التبرعات',body: 'نحن لسنا منصة لجمه التبرعات وأيضا نحن وليس لنا أي علاقة مع منصة الكترونية أو مؤسسة أو جمعية تقوم بذلك، كما اننا نتوقع منك الإبلاغ عن أي إساءة استخدام للمنصة لجمع النقود أو اقحامها في مثل هذه العمليات'),

  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kTeal400,
        elevation: 3.0,
        title: Text(
          'الاسئلة الشائعة',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),

        body: Directionality(
            textDirection: TextDirection.rtl,
            child: ListView(
              children: [
                ExpansionPanelList(
                  expansionCallback: (int index, bool isExpanded) {
                    setState(() {
                      items[index].isExpanded = !items[index].isExpanded;
                    });
                  },
                  children: items.map((ExpansionItem item) {
                    return ExpansionPanel(
                        headerBuilder: (BuildContext context, bool isExpanded) {
                          return Container(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              item.header,
                              style: TextStyle(fontSize: 19, color: kTeal400 ),
                            ),
                          );
                        },
                        isExpanded: item.isExpanded,
                        body: Container(
                            child: Text(
                              item.body,
                              style: TextStyle(fontSize: 19),
                            ),
                            padding: EdgeInsets.all(10)));
                  }).toList(),
                ),
              ],
            )));
  }
}
class ExpansionItem {
  bool isExpanded;
  final String header;
  final String body;

  ExpansionItem({this.isExpanded: false, this.header: '', this.body: ''});
}
