import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../theme/colors.dart';

class LetsHelpBlog extends StatefulWidget {
  LetsHelpBlog({Key? key}) : super(key: key);

  @override
  State<LetsHelpBlog> createState() => _LetsHelpBlogState();
}

class _LetsHelpBlogState extends State<LetsHelpBlog> {
  late ControllerPage1 controller;
  late WebViewController webViewController;
  RxBool isLoading=true.obs;
  @override
  void initState() {

    controller=Get.put(ControllerPage1());
    webViewController=WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {
            isLoading.value=false;
          },
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse('https://www.letshelp-platform.com/blog'),headers: {
        'Authorization': 'Bearer ${ controller.token!.value}'
      });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: kTeal400,
          elevation: 3.0,
          title: Text(
            'المدونة',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
         body: Obx(() {
         return isLoading.value?
         Center(
           child: CircularProgressIndicator(
             color:kTeal400 ,
           ),
         )
             :
           WebViewWidget(controller: webViewController

           );
         })
         // body: WebViewWidget(
         //   initialUrl: "https://letshelp-platform.com/blog",
         //   javascriptMode: JavascriptMode.unrestricted,
         //   onWebViewCreated: (WebViewController controller) {
         //     _controller.complete(controller);
         //   },
         // ),
      ),
    );
  }
}

class ControllerPage1 extends GetxController {
  SharedPreferences? prefs;
  RxString? token="".obs ;
  @override
  void onInit()async {
    print(token);
    prefs = await SharedPreferences.getInstance();
    token!.value = prefs!.getString('token')!;
    super.onInit();
  }
}
