class OrdersModel {
  int? id;
  String? title;
  String? description;
  String? country;
  String? city;
  String? address;
  String? client;
  String? clientImage;
  String? category;
  String? date;
  String? message=null;

  OrdersModel(
      {this.id,
        this.title,
        this.description,
        this.country,
        this.city,
        this.address,
        this.client,
        this.clientImage,
        this.category,
        this.date});

  OrdersModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    country = json['country'];
    city = json['city'];
    address = json['address'];
    client = json['client'];
    clientImage = json['client_image'];
    category = json['category'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['description'] = this.description;
    data['country'] = this.country;
    data['city'] = this.city;
    data['address'] = this.address;
    data['client'] = this.client;
    data['client_image'] = this.clientImage;
    data['category'] = this.category;
    data['date'] = this.date;
    return data;
  }
}