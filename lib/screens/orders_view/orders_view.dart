import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../services/utils.dart';
import '../../theme/colors.dart';
import 'orders_controller.dart';

class OrdersView extends StatefulWidget {
  const OrdersView({super.key, required this.orderId});
  final int orderId;

  @override
  State<OrdersView> createState() => _OrdersViewState();
}

class _OrdersViewState extends State<OrdersView> {
  late OrdersController controller;
  @override
  void initState() {
    controller=Get.put(OrdersController(orderId: widget.orderId));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(' ...فلنتساعد',style: TextStyle(color: Colors.white)),
        backgroundColor: kTeal400,
      ),
      body:
      Directionality(
        textDirection: TextDirection.rtl,
        child: Obx(() {
          return controller.orderInformation[0].message==null?
          Center(
              child: Container(
                  child: CircularProgressIndicator(
                    color: kTeal400,
                  )))
              :
            ListView(
            children: [

                 Padding(
                   padding:  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       Column(
                         children: [
                           CachedNetworkImage(
                             imageUrl: controller.orderInformation[0].clientImage!,
                             placeholder: (context,url)=>Center(
                                 child: Container(
                                     child: CircularProgressIndicator(
                                       color: kTeal400,
                                     ))) ,
                             errorWidget: (context,url,error)=>Icon(Icons.error),
                             // width: screenWidth(3),
                             height: screenHeight(10),
                           ),
                           Text(controller.orderInformation[0].client!,style: TextStyle(fontSize: screenWidth(40)),)
                         ],
                       ),
                       Text(controller.orderInformation[0].category!,style: TextStyle(color: kTeal400,),),

                       Text(controller.orderInformation[0].date!),
                     ],
                   ),
                 ),
              SizedBox(
                height: screenHeight(30),
              ),
              Center(
                child: Padding(
                  padding:  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                  child: Text(controller.orderInformation[0].title!,style: TextStyle(fontSize: screenWidth(12)),textAlign: TextAlign.center,),
                ),
              ),
              Center(
                child:Padding(
                  padding:  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
                  child: Text(controller.orderInformation[0].description!,style: TextStyle(color: Colors.grey),),
                ) ,
              ),
              SizedBox(
                height: screenHeight(30),
              ),
              Center(
                child: Text("${controller.orderInformation[0].country} , ${controller.orderInformation[0].city} , ${controller.orderInformation[0].address}"),
              ),
              SizedBox(
                height: screenHeight(30),
              ),
              Padding(
                padding:  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(3)),
                child: ElevatedButton(onPressed: (){
                  Get.defaultDialog(
                    title: "الاستجابة للطلب",
                    titleStyle: TextStyle(color: kTeal400),
                    content: Form(
                      key: controller.formKey,
                      child: Column(
                        children: [
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: TextFormField(

                              controller: controller.letterController,
                              validator: (value){
                                return value!.isEmpty? "هذا الحقل مطلوب":null;
                              },
                              decoration: InputDecoration(
                                contentPadding: EdgeInsetsDirectional.only(start: screenWidth(20)),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(screenWidth(10))
                                ),
                                hintText: "يمكنك ترك رسالة",
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 1,
                                    color: Colors.grey
                                  ),
                                  borderRadius: BorderRadius.circular(screenWidth(20))
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        width: 1,
                                        color: kTeal400
                                    ),
                                    borderRadius: BorderRadius.circular(screenWidth(20))
                                ),
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              ElevatedButton(onPressed: (){
                                if(controller.formKey.currentState!.validate())
                               controller.send(context: context);
                              },style: ElevatedButton
                        .styleFrom(

                        backgroundColor:
                        kTeal400) , child: Text("إرسال")),
                              SizedBox(
                                width: screenWidth(20),
                              ),
                              ElevatedButton(onPressed: (){
                                Get.back();
                              },style: ElevatedButton
                                  .styleFrom(

                                  backgroundColor:
                                  Colors.grey), child: Text("إغلاق")),

                            ],
                          )

                        ],
                      ),
                    ),
                  );
                },
                    style: ElevatedButton
                        .styleFrom(

                        backgroundColor:
                        kTeal400),
                    child: Text(" إجابة",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight:
                      FontWeight.bold,
                      fontSize: 23,
                      //fontFamily: 'TrajanPro' )
                    ))),
              )


            ],
          );
        }),
      ),
    );
  }
}
