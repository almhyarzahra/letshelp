import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/common_response.dart';
import '../../core/network_util.dart';
import '../../core/request_type_enum.dart';
import '../../endpoints/endpoints.dart';
import '../../endpoints/network_config.dart';
import 'orders_model.dart';

class OrdersController extends GetxController{
  OrdersController({required int orderId}){
    this.orderId.value=orderId;
  }
  RxInt orderId=0.obs;
  RxList<OrdersModel> orderInformation=<OrdersModel>[OrdersModel()].obs;
  TextEditingController letterController=TextEditingController();
  GlobalKey<FormState> formKey=GlobalKey<FormState>();

  @override
  void onInit() {
    print(orderId.value);
    getOrder();
    super.onInit();
  }

  void getOrder(){
    getOrderInformation().then((value) {
      value.fold((l) {
        getOrder();
      }, (r) {
        if(orderInformation.length>0){
          orderInformation.clear();
        }
        orderInformation.addAll(r);
        orderInformation.map((element) {
          element.message="l";
        }).toSet();
      });
    });
  }

  void send({required BuildContext context}){
    sendResponse(message:letterController.text ).then((value) {
      value.fold((l) {
        if(l==null){
          AwesomeDialog(
            context: context,
            animType: AnimType.TOPSLIDE,
            dialogType: DialogType.ERROR,
            title: "فشل العملية",
            body: Directionality(
              textDirection:
              TextDirection.rtl,
              child: Container(
                padding: EdgeInsets.all(8),
                alignment: Alignment.center,
                child: Text(
                  "حدث خطأ ما",
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 22,
                    fontWeight:
                    FontWeight.bold,
                  ),
                ),
              ),
            ),
            btnOkColor: Colors.red,
            btnOkOnPress: () {},
          ).show();
        }
        else{
          AwesomeDialog(
            context: context,
            animType: AnimType.TOPSLIDE,
            dialogType: DialogType.ERROR,
            title: "فشل العملية",
            body: Directionality(
              textDirection:
              TextDirection.rtl,
              child: Container(
                padding: EdgeInsets.all(8),
                alignment: Alignment.center,
                child: Text(
                  "لايمكن لصاحب الطلب الاستجابة لطلبه",
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 22,
                    fontWeight:
                    FontWeight.bold,
                  ),
                ),
              ),
            ),
            btnOkColor: Colors.red,
            btnOkOnPress: () {},
          ).show();
        }
      }, (r) {
        AwesomeDialog(
          context: context,
          animType: AnimType.TOPSLIDE,
          dialogType: DialogType.SUCCES,
          title: "تمت العملية",
          body: Directionality(
            textDirection:
            TextDirection.rtl,
            child: Container(
              padding: EdgeInsets.all(8),
              alignment: Alignment.center,
              child: Text(
                "نم اضافة الطلب",
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 22,
                  fontWeight:
                  FontWeight.bold,
                ),
              ),
            ),
          ),
          btnOkColor: Colors.red,
          btnOkOnPress: () {},
        ).show();
      });
    });
  }

  Future<Either<bool?, bool>> sendResponse({required String message}) async {
    try {

      SharedPreferences? _prefs = await SharedPreferences.getInstance();
      String? _token = _prefs!.getString('token');
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url:"api/orders/${orderId.value}/response-order",

        params: {
          "message":message,
        },
        headers:{
          'Authorization': 'Bearer ${_token ?? ''}',
          'Accept':'application/json',
        } ,
      ).then((response) {
        if (response == null) {
          return Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
        CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {

          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.getStatus);
        }
      });
    } catch (e) {
      return Left(e.toString() as bool?);
    }
  }


  Future<Either<String?, List<OrdersModel>>> getOrderInformation() async {
    try {
      print(EndPoints.showOrder+"/${orderId.value}/show");
      SharedPreferences? _prefs = await SharedPreferences.getInstance();
      String? _token = _prefs!.getString('token');
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url:

        EndPoints.showOrder+"/${orderId.value}/show",
        headers:{
          'Authorization': 'Bearer ${_token ?? ''}',
          'Accept':'application/json',
        } ,
      ).then((response) {
        if (response == null) {
          return Left(null);
        }
        CommonResponse<List<dynamic>> commonResponse =
        CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          List<OrdersModel> result = [];
          commonResponse.data!.forEach(
                (element) {
              result.add(OrdersModel.fromJson(element));
            },
          );
          return Right(result);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }
}