
class GoogleLoginModel {
  bool? success;
  List<Data>? data;
  Messages? messages;

  GoogleLoginModel({this.success, this.data, this.messages});

  GoogleLoginModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
    messages = json['messages'] != null
        ? new Messages.fromJson(json['messages'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    if (this.messages != null) {
      data['messages'] = this.messages!.toJson();
    }
    return data;
  }
}

class Data {
  bool? mustVerifyEmail;
  bool? mustCompleteProfile;
  String? token;
  String? name;
  String? password;

  Data({this.token, this.name, this.password,this.mustCompleteProfile,this.mustVerifyEmail});

  Data.fromJson(Map<String, dynamic> json) {
    mustCompleteProfile=json['mustCompleteProfile'];
    mustVerifyEmail=json['mustVerifyEmail'];
    token = json['token'];
    name = json['name'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['mustCompleteProfile']=this.mustCompleteProfile;
    data['mustVerifyEmail']=this.mustVerifyEmail;
    data['token'] = this.token;
    data['name'] = this.name;
    data['password'] = this.password;
    return data;
  }
}

class Messages {
  String? successMessage;

  Messages({this.successMessage});

  Messages.fromJson(Map<String, dynamic> json) {
    successMessage = json['successMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['successMessage'] = this.successMessage;
    return data;
  }
}