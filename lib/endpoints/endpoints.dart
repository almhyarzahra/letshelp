import 'package:letshelp/endpoints/network_config.dart';

class EndPoints{
  static String googleLogin=NetworkConfig.getFulApiUrl('login/google');
  static String addItems=NetworkConfig.getFulApiUrl('items/store');
  static String addOrder=NetworkConfig.getFulApiUrl('orders/store');
  static String showOrder=NetworkConfig.getFulApiUrl('orders');
  static String completeShowOrder=NetworkConfig.getFulApiUrl('show');

}