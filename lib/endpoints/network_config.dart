

import 'package:shared_preferences/shared_preferences.dart';

import '../core/request_type_enum.dart';

class NetworkConfig {
  static String BASE_API = 'api/';

  static String getFulApiUrl(String api) {
    return BASE_API + api;
  }}

//
//   static Future<Map<String, String>> getHeaders(
//       {bool isMultipartRequest = false,
//         bool? needAuth = true,
//         RequestType? type = RequestType.POST,
//         Map<String, String>? extraHeaders = const {}})async {
// SharedPreferences? _prefs = await SharedPreferences.getInstance();
// String? _token = _prefs!.getString('token');
//     return {
//       if (needAuth!) 'Authorization': 'Bearer ${_token ?? ''}',
//       if (type != RequestType.GET && isMultipartRequest == false)
//         'Content-Type': 'application/json',
//       ...extraHeaders!
//     };
//   }}
