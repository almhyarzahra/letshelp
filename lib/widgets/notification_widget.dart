import 'package:flutter/material.dart';
import 'package:letshelp/models/notification_model.dart';
import 'package:letshelp/provider/notification_provider.dart';
import 'package:letshelp/provider/order-provider.dart';
import 'package:letshelp/provider/product.dart';
import 'package:letshelp/screens/all-orders.dart';
import 'package:letshelp/screens/single-product.dart';
import 'package:letshelp/theme/colors.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../provider/categories_provider.dart';
import '../provider/messagesp_povider.dart';
import '../provider/profile-provider.dart';
import '../screens/my-items.dart';
import '../screens/privatmessag.dart';
import '../screens/requested-items-screen.dart';

class NotificationWidget extends StatelessWidget {
  const NotificationWidget({Key? key, required this.notification})
      : super(key: key);
  final NotificationModel notification;
  @override
  Widget build(BuildContext context) {
    return Consumer<NotificationProv>(
      builder: (context, nP, _) => Consumer<OrderProvider>(
        builder:(context, orderProvider, _) =>
            Consumer<MessagesProvider>  (
                builder: (context, messagesProvide, _) =>
           Consumer<ProfileProvider>(
             builder: (context, profileProvider, _) =>
              InkWell(
              onTap: () async {
                print(
                    "the notification type is : ${notification.type} ${notification.idOnPage}");
                if (notification.type == 5) {
                  final prov = await Provider.of<Product>(context, listen: false);

                  prov.getProductById(notification.idOnPage!).then((value) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SingleProductScreen(),
                      ),
                    );
                  });
                } else if (notification.type == 6) {
                  print("yse: ${notification.idOnPage}");
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => AllOrders()));
                  var orderprovider = Provider.of<OrderProvider>(
                      context,
                      listen: false);
                  orderprovider.isFirstTime = true;
                  orderprovider.cateId = 0;
                  await orderProvider.getorder(page: 1);
                  await Provider.of<CategoriesProvider>(context,
                      listen: false)
                      .getCategories();
                }
                else  {
                  SharedPreferences? pref=await SharedPreferences.getInstance();
                  final Uri _url=Uri.parse(notification.url);
                  if(!await launchUrl(_url,mode: LaunchMode.platformDefault,webViewConfiguration: WebViewConfiguration(headers:{
                    'Authorization': 'Bearer ${ pref!.getString('token')!}'
                  })));
                }
                // else if(notification.type==0){
                //   Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => PrivatMessage()),
                //   );
                //   await messagesProvide.getAllMessages();
                // }
                // else if(notification.type==1){
                //   Navigator.push(
                //       context,
                //       (MaterialPageRoute(
                //         builder: (context) =>
                //             MyItems(),
                //       )));
                // }
                // else if(notification.type==2){
                //   Navigator.push(
                //       context,
                //       (MaterialPageRoute(
                //           builder: (context) =>
                //               RequestedItemsScreen())));
                //   await profileProvider
                //       .myRequestedItem();
                // }
              },
              child: Card(
                color: notification.isRead ? Colors.white : kGrey501,
                elevation: 5,
                shadowColor:
                    notification.isRead ? Color.fromARGB(255, 71, 65, 65) : kGrey501,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ListTile(
                    title: Text(notification.text),
                    leading: Icon(Icons.notifications),
                    trailing: notification.isRead ? Text("") : Text("غير مقروء"),
                  ),
                ),
              ),
                     ),
           ),
         ),
      ),
    );
  }
}
