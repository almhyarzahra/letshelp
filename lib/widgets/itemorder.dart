import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:letshelp/provider/product.dart';
import 'package:letshelp/provider/profile-provider.dart';
import 'package:letshelp/screens/single-product.dart';
import 'package:letshelp/services/utils.dart';
import 'package:letshelp/theme/colors.dart';
import 'package:provider/provider.dart';

import '../screens/item-requestes.dart';
import '../screens/orders_view/orders_view.dart';

class ItemOrder extends StatelessWidget {
  ItemOrder({
    Key? key,

    required this.orderID,
    required this.title,
    required this.description,
    required this.country,
    required this.city,
    required this.clientName,
    required this.clientImage,
    required this.address,
    required this.category,
    required this.date,
    this.status

  }) : super(key: key);

  int orderID;
  String title;
  String description;
  String country;
  String city;
  String address;
  String clientName;
  String clientImage;
  String category;
  String date;
  int? status;
  @override
  Widget build(BuildContext context) {
    return Consumer<Product>(
      builder: (context, productProvider, _) => InkWell(
        onTap: ()  {
          // if (false) {
          //   return;
          // }
          //await productProvider.getProductById(orderID!);
          Get.to(()=>OrdersView(orderId: orderID));
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (context) => OrdersView(orderId: orderID),
          //   ),
          // );

        },
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(15),
              ),
              side: BorderSide(color: kTeal400, width: 2)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15),
                    topLeft: Radius.circular(15),
                  ),
                ),
                width: double.infinity,
                child: Image.network(
                  '$clientImage',
                  fit: BoxFit.fitWidth,
                  height: screenHeight(5),
                ),
              ),
              Container(
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "$category",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text("$title",style: TextStyle(
                        overflow: TextOverflow.ellipsis
                      ),),
                      status==0!
                          ? Consumer<ProfileProvider>(
                        builder: (context, profileProvider, _) =>
                            TextButton(
                              style: ButtonStyle(
                                  foregroundColor:
                                  MaterialStateProperty.all(kTeal400)),
                              onPressed: () async {
                                profileProvider.requestsFilter = true;
                                await profileProvider
                                    .getMyProductsRequests(orderID!);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ItemRequestes(
                                          title: title,
                                        )));
                              },
                              child: Text(
                                "عرض ",
                                style: TextStyle(color: kTeal400),
                              ),
                            ),
                      )
                          : Container(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
