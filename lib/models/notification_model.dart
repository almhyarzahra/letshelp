class NotificationModel {


  int id;
  int? type;
  int? idOnPage;
  String text;
  bool isRead;
  bool hasUrl;
  String url;


  NotificationModel({
    required this.id,
    required this.idOnPage,
  required this.text,
  required this.isRead,
    required this.type,
    required this.hasUrl,
    required this.url,

});

  factory NotificationModel.FromJson (Map<String,dynamic> json) {
  return NotificationModel(
    hasUrl: json['hasUrl'],
    url: json['url'],
    id :json["id"],
    idOnPage :json ['idOnPage'],
    text : json ["text"],
    isRead : json ["isRead"],
    type :json["type"],
  );

  }

}