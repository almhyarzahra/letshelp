class Countries {
  Countries({
    required this.id,
    required this.name,

  });

  int? id;
  String? name;


  factory Countries.fromMap(Map<String, dynamic> json) => Countries(
    id: json["id"],
    name: json["name"],

  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,

  };
}
