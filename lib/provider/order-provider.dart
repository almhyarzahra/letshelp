import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:letshelp/models/order.dart';
import 'package:http/http.dart' as http;
import 'package:letshelp/widgets/itemorder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../core/common_response.dart';
import '../core/network_util.dart';
import '../core/request_type_enum.dart';
import '../endpoints/endpoints.dart';
import '../helper/App_Bass.dart';

class OrderProvider with ChangeNotifier {
  List<ItemOrder> allOrder = [];
  var responseData;
  String addProhintList = " اختر الفئة";
  var url;
  int? categoryForFilter;
  int?countryForFilter;
  late Response response;
  String? _token;
  SharedPreferences? _prefs;
  bool isOrdersGet = true;
  bool? isCategoryChose;
  String? date;
  bool responseOrder = false;
  String? filedMessage;

  TextEditingController messageForOrderResponse = TextEditingController();
  final messageForm = GlobalKey<FormState>();
  int? orderResponseCode;
  bool? isDateChose;
  int? statusCodeAddOrder;
  TextEditingController productName = TextEditingController();
  TextEditingController country = TextEditingController();
  TextEditingController city = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController productDescription = TextEditingController();
  TextEditingController reason = TextEditingController();
  Map<String, dynamic>? para;
  int? cateId;
  bool isFirstTime = true;
  bool isInputEnable=true;
  final addOrderForm = GlobalKey<FormState>();
  Future<void> getAllOrders({int? orderId}) async {


    String apiPath = "/api/orders";
    if (orderId != null) {
      print("object");
      apiPath = "/api/orders/$orderId/show";
    } else {
      apiPath = "/api/orders";
    }
    isOrdersGet = false;
    notifyListeners();
    allOrder.clear();

    try{
      final _prefs = await SharedPreferences.getInstance();
      final _token = _prefs.get("token");
      final header = {
        "Accept": "application/json",
        "Authorization": "Bearer $_token",
      };
      url = Uri.https(APP_BASE_URL1, apiPath);
      response = await http.get(url, headers: header);
      print("Orders Status Code is : ${response.statusCode}");
      print("Orders Response is : ${response.body}");
      if (response.statusCode == 200) {
        responseData = jsonDecode(response.body);
        for (var order in responseData) {
          print("the single order is : $order");
          allOrder.add(ItemOrder(
              orderID: order['id'] ,
              title: order['title'],
              description: order['description'],
              country: order["country"],
              city: order['city'],
              clientName: order['client'],
              clientImage: order['client_image'],
              address: order['address'],
              category: order["category"],
              date: order['date']));
        }
        isOrdersGet = true;
        notifyListeners();
      }
      else {
        print("the status code of  get all order is ${response.statusCode}");
      }
    }catch (e){
      print("an error has been happen while getting all orders $e ");
    }




  }

  Future<void> orderResponse(int orderId) async {
    responseOrder = false;
    notifyListeners();
    _prefs = await SharedPreferences.getInstance();
    Map<String, String> para = {'message': '${messageForOrderResponse.text}'};
    _token = _prefs!.getString('token');
    url = Uri.https(APP_BASE_URL1, "/api/orders/$orderId/response-order", para);

    try{
      response = await http.post(url, headers: {
        'Authorization': "Bearer $_token",
        'Accept': 'Accept application/json'
      });
      orderResponseCode = response.statusCode;
      if (response.statusCode == 200) {
        print("the task of order response has been end");
        responseOrder = true;
        messageForOrderResponse.clear();
        notifyListeners();
      }
      else {
        print(" there are an error in response order  task");
        print(response.statusCode);
        print(response.body);
        responseData = jsonDecode(response.body);
        print(responseData);
        filedMessage = responseData[0]['messages']['failedMessage'];
        responseOrder = true;
        notifyListeners();
      }

    }catch(e){
      print ("an error has been happen while getting order response $e");

    }



  }
  Future<Either<String?, bool>> addOOrder({
    required String title,
    required String description,
    required int categoryId,
    required String country,
    required String city,
    required String address,

    double? alat,
    double? alng,
    double? clat,
    double? clng,

  }) async {
    try {
      SharedPreferences? _prefs = await SharedPreferences.getInstance();
      String? _token = _prefs!.getString('token');
      return NetworkUtil.sendMultipartRequest(
        type: RequestType.POST,
        url: EndPoints.addOrder,
        fields: {
          'title': title,
          'description':description,
          'category_id':categoryId.toString(),
          'country':country,
          'city':city,
          'address':address,
          if(alat!=null) 'alat':alat.toString(),
          if(alng!=null) 'alng':alng.toString(),
          if(clat!=null) 'clat':clat.toString(),
          if(clng!=null) 'clng':clng.toString()
        },

        headers:{
          'Authorization': 'Bearer ${_token ?? ''}',
          'Accept':'application/json',
        },
      ).then((response) {
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
        CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(commonResponse.getStatus);
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }


  Future<void> addOrder() async {
    _prefs = await SharedPreferences.getInstance();
    _token = _prefs!.getString('token');
    Map<String, String> queryParam = {
      'title': this.productName.text,
      'description': this.productDescription.text,
      'category_id': "1",
      'country': this.country.text,
      'city': this.city.text,
      'address': this.address.text
    };
    url = Uri.https(APP_BASE_URL1, '/api/orders/store', queryParam);

    Map<String, String> headers = {
      'Authorization': "Bearer $_token",
      "Accept": "application/json"
    };

    try{
    response = await http.post(url, headers: headers);

    if (response.statusCode != 200) {
      print(response.statusCode);
      print("Errrrrrrrrrrrrrrrrrrrrrrrrrrrrror in add order");
      statusCodeAddOrder = response.statusCode;
      print(responseData);
      filedMessage = responseData[0]['messages']['failedMessage'];
      notifyListeners();
      responseData = jsonDecode(response.body);
      print(responseData);
    }
    if (response.statusCode == 200) {
      statusCodeAddOrder = 200;

      notifyListeners();
      cleanOrderForm();
      var responseData = jsonDecode(response.body);
      print(" response of add order  body Json is :  $responseData");
    }

    }catch(e){
    print("an error has been happen while adding order  $e");

    }


  }
  void cleanOrderForm() {
    productName.clear();
    country.clear();
    city.clear();
    address.clear();
    productDescription.clear();
    reason.clear();
    notifyListeners();
  }

  Future<void> filterProduct() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    try {
      final String? _token = await _prefs.getString("token");

      para = {if(categoryForFilter!=null)
        'category_id': categoryForFilter.toString(),
        if(countryForFilter!=null)
          'country':countryForFilter.toString(),
        if(productName.text.isNotEmpty)'search':productName.text.toString(),
        if(date!=null)'date':date

      };

      var url=Uri.https('letshelp-platform.com','api/orders',para);
      // final url = Uri.parse(APP_BASE_URL + '/api/items?category_id=$categoryForFilter');
      // url = Uri.https("letshelp-platform.com", '/api/items?category_id=$categoryForFilter');
      final response = await http.get(
        url,
        headers: {"Accept": "application/json","Authorization": "Bearer $_token"},

      );
      if (response.statusCode == 200) {
        print("Response is ${response.body}");
        allOrder!.clear();
        final responseData = json.decode(response.body);
        print("ssssssssssssssss      ${responseData}");
        for (var order in responseData) {
          allOrder!.add(ItemOrder(
                        orderID: order['id'] as int,
                        title: order['title'],
                        description: order['description'],
                        country: order["country"],
                        city: order['city'],
                        clientName: order['client'],
                        clientImage: order['client_image'],
                        address: order['address'],
                        category: order["category"],
                        date: order['date']));
        }
        print("data fetched  from search  successfully");

        notifyListeners();
      } else {
        print("data fetched  from search  successfully");
        print(response.statusCode);
        notifyListeners();
      }
    } catch (e) {
      print("an error has been happen do filtering  $e   ");
    }
  }
  // Future<void> filterProduct() async {
  //   try {
  //     if (isDateChose == true) {
  //       para = {'category_id ': categoryForFilter};
  //     }
  //
  //     url = Uri.https(APP_BASE_URL1, '/api/items', para);
  //     final response = await http.post(
  //       url,
  //       headers: {"Accept": "application/json"},
  //     );
  //     if (response.statusCode == 200) {
  //       print("Response is ${response.body}");
  //       allOrder!.clear();
  //       final responseData = json.decode(response.body);
  //       print(responseData);
  //       for (var order in responseData) {
  //         allOrder.add(ItemOrder(
  //             orderID: order['id'] as int,
  //             title: order['title'],
  //             description: order['description'],
  //             country: order["country"],
  //             city: order['city'],
  //             clientName: order['client'],
  //             clientImage: order['client_image'],
  //             address: order['address'],
  //             category: order["category"],
  //             date: order['date']));
  //       }
  //       print("data fetched  from search  successfully");
  //       notifyListeners();
  //     } else {
  //       print("data fetched  from search  successfully");
  //       print(response.statusCode);
  //       notifyListeners();
  //     }
  //   } catch (e) {
  //     print("an error has been happen do filtering  $e   ");
  //   }
  // }
  Future<void> getorder({int? page}) async {
    isOrdersGet = true;
    notifyListeners();
    SharedPreferences _prefs = await SharedPreferences.getInstance();

    try {
      final String? _token = await _prefs.getString("token");

      if (cateId == 0) {
        if (isFirstTime == true) {
          allOrder!.clear();
        }
        final url = Uri.parse(APP_BASE_URL + '/api/orders?page=$page');
        final response = await http.get(
          url,
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $_token"
          },
        );
        if (response.statusCode != 200) {
          return;
        }

        if (cateId != 0) {
          allOrder!.clear();
          isFirstTime = false;
        }

        // if (page == 1) {
        //   items!.clear();
        // }

        final responseData = json.decode(response.body);

        for (var order in responseData) {
          allOrder.add(ItemOrder(
              orderID: order['id'] as int,
              title: order['title'],
              description: order['description'],
              country: order["country"],
              city: order['city'],
              clientName: order['client'],
              clientImage: order['client_image'],
              address: order['address'],
              category: order["category"],
              date: order['date']));
        }
        isOrdersGet = false;
        notifyListeners();
      } else {
        final url = Uri.parse(APP_BASE_URL + '/api/orders?category_id=$cateId');

        final response = await http.get(
          url,
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $_token"
          },
        );

        if (response.statusCode != 200) {
          print("Error fetching data");
          isOrdersGet = false;
          notifyListeners();
        }

        print("Response is ${response.body}");
        final responseData = json.decode(response.body);
        for (var order in responseData) {
          allOrder.add(ItemOrder(
              orderID: order['id'] as int,
              title: order['title'],
              description: order['description'],
              country: order["country"],
              city: order['city'],
              clientName: order['client'],
              clientImage: order['client_image'],
              address: order['address'],
              category: order["category"],
              date: order['date']));
        }
        print("data fetched successfully");
        isOrdersGet = false;
        notifyListeners();
      }
    } catch (e) {
      isOrdersGet = false;
      notifyListeners();
    }
  }

}
